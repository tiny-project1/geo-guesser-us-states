from turtle import Turtle

FONT = ("Sans courier", 8, "normal")


class Scoreboard(Turtle):
    """ Manage score """
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.penup()
        self.points = 0
        self.total_state = 50
        self.goto(x=300, y=200)
        self.write(f"{self.points}/{self.total_state}", font=FONT)

    def increase_score(self):
    """Increase the score"""
        self.points += 1

    def update_score(self):
    """Update score on screen"""
        self.clear()
        self.increase_score()
        self.write(f"{self.points}/{self.total_state}", font=FONT)
