from unittest.mock import MagicMock

import pytest

from us_states_game.score import Scoreboard

FONT = ("Sans courier", 8, "normal")


@pytest.fixture(scope="module")
def scoreboard():
    return Scoreboard()


def test_increase_score(scoreboard):
    initial_points = scoreboard.points
    scoreboard.increase_score()
    assert scoreboard.points == initial_points + 1


def test_update_score(scoreboard):
    # mock clear and write methods
    scoreboard.clear = MagicMock()
    scoreboard.write = MagicMock()

    initial_points = scoreboard.points
    scoreboard.update_score()

    # check if score is increased
    assert scoreboard.points == initial_points + 1

    # check if clear and write methods are called once with the expected arguments
    assert scoreboard.clear.call_count == 1
    assert scoreboard.write.call_count == 1
    assert (
        scoreboard.write.call_args[0][0]
        == f"{scoreboard.points}/{scoreboard.total_state}"
    )
    assert scoreboard.write.call_args[1]["font"] == FONT
