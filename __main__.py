import turtle
from turtle import Screen

import pandas
from analyser import Analyser
from score import Scoreboard

data = pandas.read_csv(r"us_states_game\50_states.csv")

screen = Screen()

screen.title("U.S States Game")
image = r"us_states_game\blank_states_img.gif"

screen.addshape(image)
turtle.shape(image)
answer = Analyser()
scoreboard = Scoreboard()

all_states_answer = []
all_states = data.state.to_list()

while len(all_states_answer) < 50:
    answer_user = screen.textinput(
        title=f"Guess the States {scoreboard.points}/{scoreboard.total_state}",
        prompt="What's an another states's name ? ",
    ).title()

    answer_state = data[data["state"] == answer_user]
    if answer_user in all_states and answer_user not in all_states_answer:
        answer.correct_answer(answer_state)
        all_states_answer.append(answer_user)
        scoreboard.update_score()

    if answer_user == "Exit":
        missing_state = [
            state for state in all_states if state not in all_states_answer
        ]
        missing_state_data = pandas.DataFrame(missing_state)
        missing_state_data.to_csv(r"us-states-game-start\states_to_learn.csv")
        break
