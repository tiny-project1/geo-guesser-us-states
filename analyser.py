from turtle import Turtle

FONT = ("Sans courier", 8, "normal")


class Analyser(Turtle):
    """Check answer user """
    def __init__(self):
        super().__init__()
        self.hideturtle()
        self.penup()

    def correct_answer(self, answer_user):
        """Write the correct answer on the map"""
        x_cor = int(answer_user.x)
        y_cor = int(answer_user.y)
        self.goto(x_cor, y_cor)
        self.write(answer_user.state.item(), font=FONT)
